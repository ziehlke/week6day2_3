/*Napisz aplikację w której:
        - wczytaj od użytkownika jedną linię tekstu
        - po wczytaniu linii otwórz plik o nazwie 'output_2.txt'
        - zapisz do pliku linię pobraną od użytkownika
        - zamknij plik*/


import java.io.*;
import java.util.Scanner;

public class Zadanie2 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("co chcesz dopisac do pliku ? -- aby zakonczyc wpisz 'quit'");


        try (PrintWriter printWriter = new PrintWriter(new FileWriter("output_2.txt", true))) {

            String linia = "";
            while (!linia.equals("quit")) {

                linia = scanner.nextLine();
                printWriter.println(linia);
                printWriter.flush();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }



/*        Należy napisać aplikację która w pętli while czyta ze Scannera wejście użytkownika z konsoli,
                a następnie linia po linii wypisuje tekst do pliku 'output_3.txt'.
            Aplikacja ma się zamykać po wpisaniu przez użytkownika komendy "quit".*/


    }


}
