/*Napisz program, a w tym programie wczytaj jedną linię ze skanera od użytkownika.
        Poproś użytkownika o 'adres pliku'. Po wpisaniu zweryfikuj czy wybrany plik/katalog istnieje,
        czy jest plikiem/katalogiem, jaki jest jego:
        rozmiar, czas ostatniej modyfikacji i czy mamy prawa do odczytu/zapisu do tego pliku/katalogu.*/


import java.io.File;
import java.text.DateFormat;
import java.util.Scanner;

public class Zadanie4 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj adres pliku: ");

        String address = scanner.nextLine();
        File file = new File(address);

        if (file.exists()) {
            System.out.println("file exists: " + file.exists());
            System.out.println("is file: " + file.isFile());
            System.out.println("is directory: " + file.isDirectory());
            double fileSize = (file.length() / 1000000.0);
            System.out.print("file size: ");
            System.out.printf("%.2f MB%n", fileSize);
//        System.out.println("last modified: " + file.lastModified() );
            System.out.println("last modified: " + DateFormat.getInstance().format(file.lastModified()));
            System.out.println("read access: " + file.canRead());
            System.out.println("write access: " + file.canWrite());
        }
        else {
            System.out.println("Files does not exist.");
        }

    }

}
