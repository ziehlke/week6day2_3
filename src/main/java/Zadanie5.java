/*Stwórz klasę formularz. Formularz reprezentuje odpowiedzi których udzielił użytkownik.
        Aplikacja ma po uruchomieniu rozpocząć od zadawania pytań użytkownikowi.

        Po wpisaniu danych(odpowiedzi na pytania) zapisz te odpowiedzi do pliku (output_form.txt)
        w odpowiednim formacie (przez format mamy na myśli zawartość - np. oddziel pytania i odpowiedzi tak,
        aby byly pisane w nowych liniach). Poproś użytkownika o: wiek, wzrost, płeć (kobieta/mezczyzna),
        zarobki i zadaj dwa dodatkowe pytania.
        Po czynności zamknij plik i program.*/


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Zadanie5 {


    public static void main(String[] args) {

        List<String> pytania = new ArrayList<>(Arrays.asList(
                "Podaj wiek: ",
                "Podaj wzrost: ",
                "Podaj zarobki: ",
                "Podaj numer buta: ",
                "Podaj kolor oczu: "
        ));


        List<String> pytaniaDlaKobiet = new ArrayList<>(Arrays.asList(
                "Wyjaśnij co to 'spalony' w piłce nożnej: ",
                "Jak nazywa się Twój kot: ",
                "Podaj ile masz torebek: ",
                "Jak czyta się Givenchy: ",
                "Kim jest Kardashian: "
        ));


        Scanner scanner = new Scanner(System.in);

        try (PrintWriter printWriter = new PrintWriter(new FileWriter("output_form.txt", true))) {

            String kontynuuj = "tak";
            while (kontynuuj.equals("tak")) {

                System.out.println("Podaj płeć: ");
                System.out.print("Kobieta / Mezczyzna: ");

                String plec = scanner.nextLine().toLowerCase();
                if (plec.equals("kobieta")) pytania = pytaniaDlaKobiet;

                for (String pytanie : pytania) {

                    System.out.println(pytanie);
                    printWriter.println(pytanie);

                    printWriter.println("- " + scanner.nextLine());
                    printWriter.println();
                }
                printWriter.println("------------------------------------------------------");
                printWriter.flush();

                System.out.println("Czy chcesz wypełnić kolejny formularz ?");
                System.out.print("tak / nie: ");

                kontynuuj = scanner.nextLine().toLowerCase();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
